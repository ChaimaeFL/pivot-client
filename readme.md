### Installation
clone this repository 
```bash

git clone git@gitlab.com:ChaimaeFL/pivot-client.git

cd pivot-client

npm install
```

## start the web application
```bash

npm start
```
