export const suppliers = [
    {
        id: 1,
        name: "L1",
        kategorieAnzahl: 3   
    },
    {
        id: 2,
        name: "L2",
        kategorieAnzahl: 4
    },
    {
        id: 3,
        name: "L3",
        kategorieAnzahl: 8
    },
    {
        id: 4,
        name: "L4",
        kategorieAnzahl: 2
    },
]