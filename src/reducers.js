import { drillUp, drillDown } from './actions.js';
import { LEVEL0 } from './constants.js';
import {
    CATEGORIES_FETCH_REQUESTED_SUCCESS,
    PRODUCTS_FETCH_REQUESTED_SUCCESS,
    CUSTOMERS_FETCH_REQUESTED_SUCCESS,
    SUPPLIERS_FETCH_REQUESTED_SUCCESS, CATEGORIES_FETCH_REQUESTED_CUSTOMERS, PRODUCTS_FETCH_REQUESTED_CUSTOMERS,
    STATISTIC_FETCH_REQUESTED_SUCCESS
} from "./types";
import {drillDownCustommer, drillUpCustomers} from "./actions";

const initState = {
    level: LEVEL0,
    levelCustomer: LEVEL0,
    chartData: null,
    chartDataByCustomer: null,
    dataStatistic: null,
    categoryId: null,
    supplierId: null,
    group: null,
}

function toStringAndCharAt(number){
    return parseInt(number.toString().charAt(0));
}

const getGroups = (customers) => {
    const customersGroups1 = customers
        .filter(item => item.orders != null && item.orders.length != 0 && toStringAndCharAt(item.address.zip) >= 6);
    const customersGroups2 = customers
        .filter(item => item.orders != null && item.orders.length != 0 && toStringAndCharAt(item.address.zip) >   3 && toStringAndCharAt(item.address.zip) <=5);
    const customersGroups3 = customers
        .filter(item => item.orders != null && item.orders.length != 0 && toStringAndCharAt(item.address.zip) <= 2);
    return {
        customersGroups1,
        customersGroups2,
        customersGroups3,
    }
}

const getChartDataByCustomer = (customers, group) => {
    const groups = getGroups(customers);
    let chartDataByCustomer = groups.customersGroups1;
    if (group === "group2")
        chartDataByCustomer = groups.customersGroups2;
    else if (group === "group3")
        chartDataByCustomer = groups.customersGroups3;
    return chartDataByCustomer
};

export default (state = initState, action) => {
    switch (action.type) {
        case drillDown:
            const newState = state.level != 2 ? {
                ...state,
                level: state.level + 1,
                supplierId: action.entry.data.supplierId ? action.entry.data.supplierId : state.supplierId,
                categoryId: action.entry.data.categoryId ? action.entry.data.categoryId : state.categoryId,
            } : {...state};
            return newState;
        case drillUp:
            return {...state, level: state.level - 1};
        case drillDownCustommer: {
            const newState = state.levelCustomer != 1 ? {
                ...state,
                levelCustomer: state.levelCustomer + 1,
                group: action.entry.name,
            } : {...state};
            return newState;
        }
        case drillUpCustomers:
            return {...state, levelCustomer: state.levelCustomer - 1};
        case SUPPLIERS_FETCH_REQUESTED_SUCCESS: {
            return {
                ...state,
                chartData: action.suppliers.map(item => ({data: item, name: item.supplierName, y: item.categoryCount})),
            };
        }
        case CATEGORIES_FETCH_REQUESTED_CUSTOMERS: {
            const chartDataByCustomer = getChartDataByCustomer(state.customers, action.group);
            let categories = {};
            const addCategory = (name) => {
                if (categories[name]) {
                    categories[name] = categories[name] +1;
                } else {
                    categories = {
                        ...categories,
                        [name]: 1
                    }
                }
            };
            chartDataByCustomer.forEach(item => {
                item.orders.forEach(order => {
                    order.orders.forEach(item => addCategory(item.category.categoryName));
                })
            });
            return {
                ...state,
                chartDataByCustomer: Object.keys(categories).map(key => ({data: key, name: key, y: categories[key]})),
            }
        }
        case STATISTIC_FETCH_REQUESTED_SUCCESS: {

            return {
                ...state,
                dataStatistic: action.statistics

            }
        }
        case CUSTOMERS_FETCH_REQUESTED_SUCCESS: {
            const groups = getGroups(action.customers);
            return {
                ...state,
                customers: action.customers,
                chartDataByCustomer: [
                    {name: "plz 6 bis 9", y: groups.customersGroups1.length},
                    {name: "plz 3 bis 5", y: groups.customersGroups2.length},
                    {name: "plz 1 und 2", y: groups.customersGroups3.length},
                    ],
            };
        }
        case CATEGORIES_FETCH_REQUESTED_SUCCESS:
            return {
                ...state,
                chartData: action.categories.map(item => ({data: item, name: item.categoryName, y: item.productCount})),
            };
        case PRODUCTS_FETCH_REQUESTED_SUCCESS:
            return {
                ...state,
                chartData: action.products.map(item => ({data: item, name: item.productName, y: item.price})),
            };
        default:
            return {
                ...state
            };
    }
  };