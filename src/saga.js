import { call, put, all, takeLatest } from 'redux-saga/effects'
import Api from './rest-client'
import {
    CATEGORIES_FETCH_REQUESTED, CUSTOMERS_FETCH_REQUESTED, PRODUCTS_FETCH_REQUESTED, STATISTIC_FETCH_REQUESTED,
    SUPPLIERS_FETCH_REQUESTED
} from "./types";
import {
    fetchCategoriesSuccess, fetchCustomersSuccess, fetchProductsSuccess, fetchStatisticSuccess,
    fetchSuppliersSuccess
} from './actions';

function* fetchSuppliers(action) {
    const data = yield call(Api.getSuppliers);
    yield put(fetchSuppliersSuccess(data));
}

function* watchFetchSuppliers() {
    yield takeLatest(SUPPLIERS_FETCH_REQUESTED, fetchSuppliers);
}

function* fetchCategories(action) {
    const categories = yield call(Api.getCategories, action.supplierId);
    yield put(fetchCategoriesSuccess(categories));
}

function* watchFetchCategories() {
    yield takeLatest(CATEGORIES_FETCH_REQUESTED, fetchCategories);
}

function* fetchProducts(action) {
    const products = yield call(Api.getProducts, action.supplierId, action.categoryId);
    yield put(fetchProductsSuccess(products));
}

function* fetchCustomers(action) {
    const customers = yield call(Api.getCustomers);
    yield put(fetchCustomersSuccess(customers));
}

function* fetchStatistic(action) {
    const statistics = yield call(Api.getStatistic);
    yield put(fetchStatisticSuccess(statistics));
}

function* watchFetchProducts() {
    yield takeLatest(PRODUCTS_FETCH_REQUESTED, fetchProducts);
}

function* watchFetchCustomers() {
    yield takeLatest(CUSTOMERS_FETCH_REQUESTED, fetchCustomers);
}

function* watchFetchStatistic() {
    yield takeLatest(STATISTIC_FETCH_REQUESTED, fetchStatistic);
}

function* rootSaga() {
    yield all([
        watchFetchSuppliers(),
        watchFetchProducts(),
        watchFetchCategories(),
        watchFetchCustomers(),
        watchFetchStatistic()
    ])
}

export default rootSaga;