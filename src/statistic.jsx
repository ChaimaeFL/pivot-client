import React from 'react';

export default (props) => {
  const {data} = props;
  
  return (
      <table>
        <tr>
          <th>name</th>
          <th>value</th>
        </tr>

          {
              data &&
            data.map(item => {
               return <tr>
               <td>{item.name}</td>
               <td>{item.value}</td>
               </tr>
            })
          }


      </table>
  )
}