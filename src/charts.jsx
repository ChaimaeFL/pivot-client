import React from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import {drillDownAction } from './actions';

export default (props) => {
  const {chartData, chartMetaData, dispatch} = props;

  const options = {
    chart: {
      type: 'column'
    },
    title: {
      text: chartMetaData.title
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: chartMetaData.yAxisTitle,
      }
  },
  plotOptions: {
    series: {
        cursor: 'pointer',
        events: {
          click: function (event) {          
            dispatch(drillDownAction(event.point));
        }
        }
    }
},
  series: [{
    name: chartMetaData.xAxisTitle,
    data: chartData
  }]
}
  
  return (
    <HighchartsReact
    highcharts={Highcharts}
    options={options}
  />
  )
}