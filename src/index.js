import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import Charts from './charts.jsx';
import Statistic from './statistic.jsx';
import CustomersCharts from './customers-charts.jsx';
import { LEVEL1, LEVEL2, LEVEL0 } from './constants.js';
import { drillUpAction } from './actions.js';
import { createStore, applyMiddleware } from 'redux';
import { Provider, useDispatch, useSelector } from 'react-redux'
import reducer from './reducers';
import rootSaga from './saga';
import createSagaMiddleware from 'redux-saga'
import {
    drillUpCustomersAction, fetchCategories, fetchCategoriesCustomers, fetchCustomers, fetchProducts,
    fetchProductsCustomers, fetchStatistic,
    fetchSuppliers
} from "./actions";

const getChartMetaData = (level) => {
  switch(level) {
    case LEVEL0:
      return {
        title: "Suppliers by Categories",
        yAxisTitle: "Categorie",
        xAxisTitle: "Supplier"
      };
    case LEVEL1:
      return {
        title: "Categories by Product",
        yAxisTitle: "Product",
        xAxisTitle: "Categorie"
      };
    case LEVEL2:
      return {
        title: "Products by Price",
        yAxisTitle: "price",
        xAxisTitle: "Products"
      };
    default:
      throw new Error();
  }
};

const getChartMetaDataByCustomer = (level, title) => {
    switch(level) {
        case LEVEL0:
            return {
                title: "Customers by Orders",
                yAxisTitle: "Orders",
                xAxisTitle: "Customers"
            };
        case LEVEL1:
            return {
                title,
                yAxisTitle: "Product",
                xAxisTitle: "Categorie"
            };
        default:
            throw new Error();
    }
};

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(rootSaga);

const App = () => {
  const state = useSelector(state => state);
  const {level, levelCustomer, chartData, chartDataByCustomer, dataStatistic} = state;
  const dispatch = useDispatch();
  const chartMetaData = getChartMetaData(level);
  const chartMetaDataByCustomer = getChartMetaDataByCustomer(levelCustomer, state.group);
  useEffect(() => {
      dispatch(fetchStatistic());
    switch(level) {
        case LEVEL0: {
            dispatch(fetchSuppliers());
            break;
        }
        case LEVEL1:
            dispatch(fetchCategories(state.supplierId));
            break;
        case LEVEL2:
            dispatch(fetchProducts(state.supplierId, state.categoryId));
            break;
    }
      switch(levelCustomer) {
          case LEVEL0: {
              dispatch(fetchCustomers());
              break;
          }
          case LEVEL1:
              dispatch(fetchCategoriesCustomers(state.group));
              break;
          case LEVEL2:
              dispatch(fetchProductsCustomers(state.group));
              break;
      }
  }, [level, levelCustomer]);
  return <>  
    <Statistic data={dataStatistic}/>
  <br/>
  <br/>
  <br/>
  {state.level !== LEVEL0 && <button onClick={() => dispatch(drillUpAction())}>Zurück</button>}
  <Charts dispatch={dispatch} chartData={chartData} chartMetaData={chartMetaData}/>
    {state.levelCustomer !== LEVEL0 && <button onClick={() => dispatch(drillUpCustomersAction())}>Zurück</button>}
  <CustomersCharts level={levelCustomer} dispatch={dispatch} chartData={chartDataByCustomer} chartMetaData={chartMetaDataByCustomer}/>
  </>;
}

ReactDOM.render(<Provider store = {store}><App /></Provider>, document.getElementById('root'));