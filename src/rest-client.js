
const baseUrl = "http://localhost:8080/api/";


async function getSuppliers()
{
    let response = await fetch(`${baseUrl}suppliers/total-categories`);
    return response.json();
}

async function getCategories(supplierId) {
    let response = await fetch(`${baseUrl}categories/${supplierId}/category-count`);
    return response.json();
};

async function getProducts (supplierId, categorieId) {
    let response = await fetch(`${baseUrl}products/${supplierId}/${categorieId}`);
    return response.json();
}

async function getCustomers () {
    let response = await fetch(`${baseUrl}customers/all`);
    return response.json();
}

async function getStatistic () {
    let responseCustomerCount = await fetch(`${baseUrl}customers/customer-count`);
    let responseSupplierCount = await fetch(`${baseUrl}suppliers/supplier-count`);
    let responseOrderCount = await fetch(`${baseUrl}orders/order-count`);
    let responseCategoryCount = await fetch(`${baseUrl}categories/category-count`);
    let responseProductCount = await fetch(`${baseUrl}products/product-count`);
    const customerCount = await responseCustomerCount.json();
     const   supplierCount= await responseSupplierCount.json();
        const orderCount= await responseOrderCount.json();
        const categoryCount= await responseCategoryCount.json();
        const productCount=await responseProductCount.json();
    return [
        {name: "customers", value: customerCount},
        {name: "suppliers", value: supplierCount},
        {name: "orders", value: orderCount},
        {name: "categories", value: categoryCount},
        {name: "products", value: productCount},
    ]
}

const Api = {
    getSuppliers,
    getCategories,
    getProducts,
    getCustomers,
    getStatistic
};

export default Api;