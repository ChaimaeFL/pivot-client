import React from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import {drillDownCustomersAction} from './actions';

export default (props) => {
  const {level, chartData, chartMetaData, dispatch} = props;

  const options = {
    chart: {
      type: 'pie'
    },
    title: {
      text: chartMetaData.title
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: chartMetaData.yAxisTitle,
      },
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: false
          },
          showInLegend: true
      },
    series: {
        cursor: 'pointer',
        events: {
          click: function (event) {          
            dispatch(drillDownCustomersAction(event.point));
        }
        }
    }
},
  series: [{
    name: chartMetaData.xAxisTitle,
    data: chartData
  }]
}
  
  return (
    <HighchartsReact
    highcharts={Highcharts}
    options={options}
  />
  )
}