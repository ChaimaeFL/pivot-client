import {
    CATEGORIES_FETCH_REQUESTED, CATEGORIES_FETCH_REQUESTED_CUSTOMERS,
    CATEGORIES_FETCH_REQUESTED_SUCCESS, CUSTOMERS_FETCH_REQUESTED, CUSTOMERS_FETCH_REQUESTED_SUCCESS,
    PRODUCTS_FETCH_REQUESTED, PRODUCTS_FETCH_REQUESTED_CUSTOMERS,
    PRODUCTS_FETCH_REQUESTED_SUCCESS, STATISTIC_FETCH_REQUESTED, STATISTIC_FETCH_REQUESTED_SUCCESS,
    SUPPLIERS_FETCH_REQUESTED, SUPPLIERS_FETCH_REQUESTED_SUCCESS
} from "./types";

export const drillUp= "drillUp";
export const drillUpCustomers= "drillUpCustomers";
export const drillDownCustommer = "drillDownCustommer";
export const drillDown = "drillDown";
export const drillDownAction = (entry) => ({type: drillDown, entry})            ;
export const drillDownCustomersAction = (entry) => ({type: drillDownCustommer, entry})            ;
export const drillUpAction = () => ({type: drillUp});
export const drillUpCustomersAction = () => ({type: drillUpCustomers});
export const fetchSuppliers = () => ({type: SUPPLIERS_FETCH_REQUESTED});
export const fetchStatistic = () => ({type: STATISTIC_FETCH_REQUESTED});
export const fetchSuppliersSuccess = (suppliers) => ({type: SUPPLIERS_FETCH_REQUESTED_SUCCESS, suppliers});
export const fetchCategories = (supplierId) => ({type: CATEGORIES_FETCH_REQUESTED, supplierId});
export const fetchCategoriesCustomers = (group) => ({type: CATEGORIES_FETCH_REQUESTED_CUSTOMERS, group});
export const fetchCategoriesSuccess = (categories) => ({type: CATEGORIES_FETCH_REQUESTED_SUCCESS, categories});
export const fetchProducts = (supplierId, categoryId) => ({type: PRODUCTS_FETCH_REQUESTED,supplierId, categoryId });
export const fetchProductsSuccess = (products) => ({type: PRODUCTS_FETCH_REQUESTED_SUCCESS, products});
export const fetchCustomers = () => ({type: CUSTOMERS_FETCH_REQUESTED });
export const fetchCustomersSuccess = (customers) => ({type: CUSTOMERS_FETCH_REQUESTED_SUCCESS, customers});
export const fetchStatisticSuccess = (statistics) => ({type: STATISTIC_FETCH_REQUESTED_SUCCESS, statistics});
export const fetchProductsCustomers = (group) => ({type: PRODUCTS_FETCH_REQUESTED_CUSTOMERS, group});
